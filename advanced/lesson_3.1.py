import gevent
from gevent import monkey
# monkey.patch_all()


def powerfull_operation():
    print('start')
    gevent.sleep(5)
    print('finish')


jobs = [
    gevent.spawn(powerfull_operation),
    gevent.spawn(powerfull_operation),
    gevent.spawn(powerfull_operation),
    gevent.spawn(powerfull_operation),
    gevent.spawn(powerfull_operation),
]

gevent.joinall(jobs)



