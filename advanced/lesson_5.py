import numpy

my_arr = numpy.array([[2, 2, 3, 3], [3, -10, 5, -1], [6, 7, 8, 7], [9, 10, -88, 1]], dtype=numpy.int8)


print(my_arr)

min = 999

for i in range(0, 4):
    for j in range(0, 3-i):
        if my_arr[i][j] < min:
            min = my_arr[i][j]

print(min)


# for i in range(1,4):
#     for j in range(3, 3-i, -1):
#         if my_arr[i][j] < min:
#             min = my_arr[i][j]

# print(min)


var = [1,2,3,'q',1]
print(var)