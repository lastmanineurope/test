from typing import List


class Directory:
    def __init__(self, name: str, root: 'Directory', files: List['File', ...], sub_directories: List['Directory', ...]):
        self.name = name
        self.root = root
        self.files = files
        self.sub_directories = sub_directories

    def add_sub_directory(self, new_directory: 'Directory'):
         new_directory.root = self.root

    def remove_sub_directory(self: 'Directory'):
        self.root = []









class File:
    def __init__(self, name: str, directory: Directory):
        self.name = name
        self.directory = directory


