import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(b'Test message', ('127.0.0.1', 8888))

result = sock.recv(1024)
print('Message', result.decode('utf-8'))