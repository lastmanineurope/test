import requests

class MyHTTPHelper():

    def send_get_request(self, url, parameters=None, headers=None):
        response = requests.get(url=url, params=parameters, headers=headers)
        return response

    def send_post_request(self, url, json, parameters=None, headers=None):
        response = requests.post(url=url, json=json, params=parameters, headers=headers)
        return response

    def send_put_request(self, url, parameters=None, headers=None):
        response = requests.put(url=url, params=parameters, headers=headers)
        return response

    def send_patch_request(self, url, parameters=None, headers=None):
        response = requests.patch(url=url, params=parameters, headers=headers)
        return response

    def send_delete_request(self, url, parameters=None, headers=None):
        response = requests.delete(url=url, params=parameters, headers=headers)
        return response

