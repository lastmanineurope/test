import os
import socket
import time

unix_sock_name = 'unix.sock'
sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

if os.path.exists(unix_sock_name):
    os.remove(unix_sock_name)

sock.bind(unix_sock_name)

# while True:
#     try:
#         result = sock.recv(1024)
#     except KeyboardInterrupt:
#         sock.close()
#         break
#     else:
#         my_list = list(x for x in result.decode('utf-8') if x.isdigit())
#         my_sum = int(my_list[0]) + int(my_list[1])
#
#         print(my_sum)
#         print('Message', result.decode('utf-8'))
#         break

result = sock.recv(1024)
print("sds")
time.sleep(5)
sock.sendto(b'Test message', unix_sock_name)