# task 1
def hello(name):
    if name == '':
        name = 'Aleksandr'
    return (f'Hello, {name}')


print(hello(''))


# task 3
def my_addition():
    y_or_n = 'y'
    while y_or_n == 'y':
        x = int(input('x: '))
        y = int(input('y: '))
        result = x + y
        print(result)
        y_or_n = input('continue? ')


def my_subtraction():
    y_or_n = 'y'
    while y_or_n == 'y':
        x = int(input('x: '))
        y = int(input('y: '))
        result = x - y
        print(result)
        y_or_n = input('continue? ')


def my_multiplication():
    y_or_n = 'y'
    while y_or_n == 'y':
        x = int(input('x: '))
        y = int(input('y: '))
        result = x * y
        print(result)
        y_or_n = input('continue? ')


def my_division():
    y_or_n = 'y'
    while y_or_n == 'y':
        x = int(input('x: '))
        y = int(input('y: '))
        if y == 0:
            print('you are trying divide by zero')
            continue
        result = x / y
        print(result)
        y_or_n = input('continue? ')

my_division()


# extra

def my_avarage(x, y, z):
    return ((x + y + z) / 3)


list_of_numbers = []
input_num = 0
for numbers in range(3):
    input_num = int(input(f'enter number '))
    list_of_numbers.append(input_num)
x = list_of_numbers[0]
y = list_of_numbers[1]
z = list_of_numbers[2]
print(my_avarage(x, y, z))