#task 1
def my_function (*x):
    my_list1 = [y for y in x]

    list_len = len(my_list1)
    answer = sum(my_list1) / list_len
    return answer


print(my_function(6,4))


#task 3
list2 = []
while True:
    word = (input('Enter word:'))
    if word != 'exit':
        list2.append(word)
    else:
        break

for x in list2:
    y = sorted(x)
    print(y)


#extra
my_list = []
while True:
    my_list.append(input('Enter number: '))
    answer = input('Continue? ')
    if answer == 'n':
        break

print(my_list)
my_list.sort()
print(my_list)
