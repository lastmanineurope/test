# task 1
class Book:
    def __init__(self, author, title, year, genre):
        self.author = author
        self.name = title
        self.year = year
        self.genre = genre

    list_of_review = 0

    def __eq__(self, other):
        return self.author == other.author and self.name == other.name and\
               self.year == other.year and self.genre == other.genre

    def __repr__(self):
        return self.author

    def __str__(self):
        return self.author


# task 2
class Review:
    def __init__(self, author, book, mark, review):
        self.author = author
        self.book = book
        self.mark = mark
        self.review = review


book1 = Book('Kolya', 'Qwe', 1988, 'Drama')
review1 = Review ('me', book1, 5, 'Not Bad')

print(review1.author)

book2 = Book('Kolya', 'Qwe', 1988, 'Drama')
book3 = Book('Nadya', 'Tst', 1991, 'Comedy')


print(book1 == book3)


# task 3
class MyTemperature:
    def __init__(self):
        self.__farengate = 10
        self.__celsius = 10

    def transform_to(self, value):
        if value == 'F':
            self.farengate = (self.celsius * (9/5)) + 32
            return self.farengate
        elif value == 'C':
            self.celsius = (self.farengate - 32) * (5/9)
            return self.celsius

    @property
    def farengate(self):
        return self.__farengate

    @property
    def celsius(self):
        return self.__celsius

    @farengate.setter
    def farengate(self, value):
        self.__farengate = value

    @celsius.setter
    def celsius(self, value):
        self.__celsius = value





obj = MyTemperature()


obj.farengate = 100
obj.transform_to('C')
print(obj.celsius)


obj.celsius = 7
obj.transform_to('F')
print(obj.farengate)




def test():
    return 'Hello world'