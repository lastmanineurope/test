class Employee:
    def __init__(self, name, surname, year, department):

        try:
            self.name = name
            self.surname = surname
            self.year = year
            self.department = department
        except EOFError:
            pass

    def sortData(self, year):
        if self.year > year:
            print(self.name)


kolya = Employee('Kolya', 'Ivanov', 1973, 'Finance')
stepa = Employee('Stepa', 'Petrov', 1988, 'Staff')

year = int(input("Enter year: "))
kolya.sortData(year)
stepa.sortData(year)
