# Task 1
class Editor:
    def view_document(self):
        self.document = 'Default document'
        return self.document

    def edit_document(self):
        return "Editing function is unavailable in free version"


class ProEditor(Editor):
    def edit_document(self):
        return "Editing function is available"


code = int(input('Enter licence code: '))

if code == 12345:
    pro = ProEditor()
    print(pro.view_document())
    print(pro.edit_document())
else:
    print('Wrong code. Only free version available:')
    free = Editor()
    print(free.view_document())
    print(free.edit_document())


#task 2

class GraphicObject:
    pass


class Rectangle(GraphicObject):
    def __init__(self, size):
        self.size = size


class MouseHandler:
    pass

    def mouse_click(self):
        return f'click on {self}'


class Button(MouseHandler):
    pass


button = Button().mouse_click()
rectangle = Rectangle(5)

# task 3
class Base:
    def method(self):
        print('Base')

class A(Base):
    def method(self):
        print('A')


class B(Base):
    def method(self):
        print('B')



class C(A, B):
    pass
    #def method(self):
        #print('C')


c = C()
c.method()
