# task 1
set_one = {1,2,3,4,5}
set_two = {4,5,6,7,8}

x = set_one.intersection(set_two)

print(x)



links = {}


def my_links():
    while True:
        new_link = {input('Enter short name: '): input('Enter long name: ')}
        links.update(new_link)

        answer = input('Continue? ')
        if answer == 'no' or answer == 'exit':
            break
        else:
            continue
    return links


def get_full_link(key):
    value = links.get(key)
    if value == None:
        return 'key bot found'
    else:
        return value