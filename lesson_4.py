import random
import essential.lesson_1

essential.lesson_1.test()
pass


# task 1
a = int(input('number a: '))
b = int(input('number b: '))

print(a, b)
sum = 0

if a < b:
    for number in range(a, b+1):
        x = sum + number
    print(sum)
else:
    print('out of range')


# task 2
x = 1
factorial = random.randrange(1, 6)
print(factorial)

for number in range(2, factorial+1):
    x = x * number
print(x)


# task 3
x = '*'

for num in range(1, 11):
    print()
    for numx in range(num):
        print('x', end=' ')


# extra
star = '*'
range1 = int(input('Enter range 1: '))
range2 = int(input('Enter range 2: '))

for column in range(range1, range2):
    print()
    for line in range(range1, range2):
        print(star, end='')


