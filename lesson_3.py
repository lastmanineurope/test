import math
from math import cos
from math import pi


# task2
x = float(input('Enter x: '))
y = 0

if -pi <= x <= pi:
    y = cos(3 * x)
elif x < -pi or x > pi:
    y = x

print(y)


# task 3
a = int(input('Enter a: '))
b = int(input('Enter b: '))
c = int(input('Enter c: '))


discriminant = b ** 2 - (4 * a * c)
print(f'Your discriminant is {discriminant}')

if discriminant < 0:
    print("discriminant less than 0")
elif discriminant > 0:
    x1 = (-b + math.sqrt(discriminant)) / (2 * a)
    x2 = (-b - math.sqrt(discriminant)) / (2 * a)
    print(f'x = {x1}')
    print(f'x = {x2}')
else:
    x = -(b/(2*a))
    print(f'x = {x}')


# extra
class Calc():

    def addition(self, x, y):
        sum = x + y
        return sum

    def subtraction(self, x, y):
        sum = x - y
        return sum

    def multiplication(self, x, y):
        sum = x * y
        return sum

    def division(self, x, y):
        sum = x / y
        return sum

    def power(self, x, n):
        sum = x ** n
        return sum


x = Calc()
print(x.multiplication(12, 2))
print(x.addition(12, 2))